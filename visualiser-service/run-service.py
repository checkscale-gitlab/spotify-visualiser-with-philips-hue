import os
import sys
import time
from concurrent.futures import ThreadPoolExecutor
from enum import Enum
from multiprocessing import Pipe
from random import randrange
from threading import Thread

import eventlet.wsgi
import requests
import socketio


class SocketCommand(Enum):
    """
    Defines each command that can be sent from the client.
    """
    CMD_CONNECT = 1
    CMD_CONFIG = 2
    CMD_START = 3
    CMD_STOP = 4
    CMD_DISCONNECT = 5


class VisualisationType(Enum):
    """
    Defines each visualisation type supported by the visualiser.
    Note the member names are important, they must match the front end visualisation type names.
    """
    ON_BEAT = 1


class Track:
    """
    A data structure to store information about a track.
    """

    def __init__(self, track_id=None, progress=0, beats=None):
        """
        Initialises the track.
        :param track_id: The unique string identifier fr the track.
        :param progress: The progress through the track in milliseconds (integer).
        :param beats: A list of integers representing the millisecond timestamps of each beat in the track.
        """
        if beats is None:
            beats = []
        self.track_id = track_id
        self.progress = progress
        self.beats = beats

    def get_beats_after(self, timestamp):
        """
        Returns the beat timestamps on and after the specified point in the track.
        :param timestamp: The point in the track to gather the beats after.
        :return: A list of integer timestamps.
        """
        return [beat for beat in self.beats if beat >= timestamp]


def call_hue_service(bridge_ip, username, method, endpoint, body=None):
    """
    Makes a HTTP request to the hue bridge, given the IP address of the bridge and username.
    :param bridge_ip: The IP of the hue bridge as a string.
    :param username: A valid username credential for the bridge API.
    :param method: Type of HTTP request as a string, e.g. GET.
    :param endpoint: The endpoint to make the request to (as a string).
    :param body: The body of the request (assumed to be in a json format).
    :return: The json response and status code from the hue bridge.
    """
    response = requests.request(method, 'http://hue-service:80/{}'.format(endpoint), json=body,
                                headers={'Hue-Bridge-Ip': bridge_ip, 'Authorization': username})
    return response.json(), response.status_code


def call_spotify_service(access_token, method, endpoint, body=None):
    """
    Makes a HTTP request to the spotify API, given a valid access token.
    :param access_token: The access token to the spotify API as a string.
    :param method: Type of HTTP request as a string, e.g. GET.
    :param endpoint: The endpoint to make the request to (as a string).
    :param body: The body of the request (assumed to be in a json format).
    :return: The json response and status code from the spotify api.
    """
    response = requests.request(method, 'http://spotify-service:80/{}'.format(endpoint),
                                headers={'Authorization': 'Bearer {}'.format(access_token)}, json=body)
    if response.status_code == 204:
        json_response = {}
    else:
        json_response = response.json()
    return json_response, response.status_code


def current_time_ms():
    """
    Returns the current time in milliseconds, as an integer rounded to the nearest millisecond.
    :return: Millisecond time.
    """
    return int(round(time.time() * 1000))


def visualisation_loop(pipe_connection, emit_to_client):
    """
    A control loop for the visualisation process.
    :param pipe_connection: A Pipe object where commands and data are sent to.
    :param emit_to_client: A function to call when emitting an event to the client. First argument is the event name and
    the second is the data to emit.
    """
    sid = None
    client_connected = True
    visualisation_running = False

    authentication_data = None
    light_ids = None
    visualisation_type = None
    offset = None
    brightness = None

    current_track = Track()
    time_at_play = 0
    upcoming_beats = []

    while client_connected:
        if pipe_connection.poll(0.01):
            command, data = pipe_connection.recv()
            if os.environ['DEBUG'] == 'true':
                if sid:
                    print('{}: {} {}'.format(sid, command.name, data))
                else:
                    print('{} {}'.format(command.name, data))
                sys.stdout.flush()

            if command is SocketCommand.CMD_CONNECT:
                sid = data

            elif command is SocketCommand.CMD_CONFIG:
                authentication_data = data['authentication']
                light_ids = data['lights']
                visualisation_type = data['visualisationType']
                offset = data['offset']
                brightness = data['brightness']

            elif command is SocketCommand.CMD_START:
                current_track.track_id = data['track_id']
                current_track.progress = data['progress']
                current_track.beats = data['audio_analysis']['beats']
                upcoming_beats = current_track.get_beats_after(current_track.progress + offset)
                time_at_play = current_time_ms()
                visualisation_running = True

            elif command is SocketCommand.CMD_STOP:
                visualisation_running = False

            elif command is SocketCommand.CMD_DISCONNECT:
                visualisation_running = False
                client_connected = False

        if visualisation_running and upcoming_beats:
            if upcoming_beats[0] <= current_track.progress + offset + (current_time_ms() - time_at_play):
                upcoming_beats.pop(0)
                if visualisation_type == VisualisationType.ON_BEAT.name:
                    with ThreadPoolExecutor(max_workers=len(light_ids)) as executor:
                        hue = randrange(65535)
                        for light in light_ids:
                            executor.submit(call_hue_service, authentication_data['hue_bridge_ip'],
                                            authentication_data['hue_username'], 'PUT', 'lights/{}'.format(light),
                                            {'hue': hue, 'transitiontime': 0, 'bri': brightness})


def init_stream_handlers(socket, threads):
    """
    Defines the actions taken when receiving events from clients.
    :param socket: The socket.io object connections are received through.
    :param threads: A dictionary of tracking each open socket stream, with sid being used for the key.
    """

    @socket.event
    def connect(sid, env):
        if sid in threads:
            return False
        conn1, conn2 = Pipe()
        threads[sid] = (Thread(target=visualisation_loop,
                               args=(conn1, lambda event, data: socket.emit(event, data, sid))),
                        conn2)
        threads[sid][0].start()
        threads[sid][1].send((SocketCommand.CMD_CONNECT, sid))

    @socket.event
    def disconnect(sid):
        threads[sid][1].send((SocketCommand.CMD_DISCONNECT, ''))
        threads.pop(sid)

    @socket.event
    def config(sid, data):
        threads[sid][1].send((SocketCommand.CMD_CONFIG, data))

    @socket.event
    def start(sid, data):
        threads[sid][1].send((SocketCommand.CMD_START, data))

    @socket.event
    def stop(sid, data):
        threads[sid][1].send((SocketCommand.CMD_STOP, ''))


if __name__ == '__main__':
    # Required to get threads and client sessions to work with each other.
    eventlet.monkey_patch()

    # Tracks user sessions.
    session_threads = {}

    # Socket.io setup.
    sio = socketio.Server(cors_allowed_origins='*')
    app = socketio.WSGIApp(sio)
    init_stream_handlers(sio, session_threads)

    # Launch the web server listening for stream connections.
    eventlet.wsgi.server(eventlet.listen(('0.0.0.0', 17002)), app)
