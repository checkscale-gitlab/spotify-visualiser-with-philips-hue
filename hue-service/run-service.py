import os
import sys

import requests
from flask import Flask, request
from flask_cors import CORS
from flask_restful import Api, Resource
from requests import ConnectTimeout, ReadTimeout


def ensure_authorisation_parameters_present():
    """
    Called before every request to ensure the caller is authorised.
    :return: An error if the caller is unauthorised, otherwise None.
    """
    if not request.method == 'OPTIONS':
        if 'Hue-Bridge-Ip' not in request.headers:
            return {'error': 'Hue-Bridge-Ip header not found in request'}, 400
        # Don't check for authorisation if the authenticate endpoint is being called.
        if '/' + str(request.endpoint) != ENDPOINT_AUTHENTICATE:
            if 'Authorization' not in request.headers:
                return {'error': 'Authorization header not found in request'}, 400
            response, status = make_request_to_bridge('GET', 'lights')
            if is_response_error(response):
                if status == 504:
                    return response, status
                return {'error': 'Hue is unauthorised'}, 401


def make_request_to_bridge(method, endpoint, body=None):
    """
    Makes a HTTP request to the hue bridge, given the IP address found during discovery and username from the request
    parameters.
    :param method: Type of HTTP request as a string, e.g. GET.
    :param endpoint: The endpoint to make the request to (as a string).
    :param body: The body of the request (assumed to be in a json format).
    :return: The json response and status code from the hue bridge.
    """
    try:
        response = requests.request(method, 'http://{}/api/{}/{}'.format(request.headers['Hue-Bridge-Ip'],
                                                                         request.headers['Authorization'],
                                                                         endpoint),
                                    json=body, timeout=BRIDGE_REQUEST_TIMEOUT)
    except (ReadTimeout, ConnectTimeout):
        return timeout_error()
    return response.json(), response.status_code


def timeout_error():
    """
    Called when a timeout error occurs.
    :return: An error to indicate the timeout.
    """
    return {'error': 'Could not connect to the Hue bridge'}, 504


def is_response_error(response):
    """
    Checks whether the response from the bridge indicates an error occurred.
    :param response: The json response to check.
    :return: True if response represents an error, False otherwise.
    """
    return response is None or 'error' in response or (isinstance(response, list) and 'error' in response[0])


def print_responses(response):
    """
    If debug mode is on then the JSON API response is printed to the console.
    :param response: The Flask response object.
    :return: The same response object, unmodified.
    """
    if os.environ['DEBUG'] == 'true' and response.content_type == 'application/json':
        print('\n\n' + response.get_data(True))
        sys.stdout.flush()
    return response


class Authenticate(Resource):
    """
    Authentication endpoint.
    """

    def get(self):
        """
        The GET endpoint.
        Creates a new username which has been authenticated.
        This endpoint may be required to be called multiple times due to the physical security of the philips hue
        bridge, which requires the link button to be pressed and this endpoint to be called again within 30 seconds of
        the original call to this endpoint.
        :return: The bridge IP address and username if successfully authenticated, otherwise an error.
        """
        try:
            response = requests.post('http://{}/api'.format(request.headers['Hue-Bridge-Ip']),
                                     json={'devicetype': 'spotify-visualiser-with-philips-hue'},
                                     timeout=BRIDGE_REQUEST_TIMEOUT).json()
        except (ReadTimeout, ConnectTimeout):
            return timeout_error()

        # May have to physically press the link button on the bridge.
        if 'error' in response[0]:
            return {'error': 'Press the link button on the hue and try again within the next 30 seconds'}, 200

        # Return username on success.
        return {'status': 'Authenticated successfully',
                'bridge_ip_address': request.headers['Hue-Bridge-Ip'],
                'username': response[0]['success']['username']}, 200


class CheckAuthenticated(Resource):
    """
    Check authenticated endpoint.
    """

    def get(self):
        """
        The GET endpoint.
        Checks if the credentials sent with this request are authenticated.
        :return: Success if credentials are correct, otherwise error.
        """
        # If control reaches here then the credentials have already been checked and are correct, therefore return
        # success.
        return {'status': 'Credentials are authenticated'}, 200


class Rooms(Resource):
    """
    Rooms endpoint.
    """

    def get(self):
        """
        The GET endpoint.
        Retrieves all the lights connected to the bridge and returns them in groups corresponding to the rooms they are
        assigned to.
        :return: The lights collected into groups or an error if one occurs.
        """
        # Get the group information.
        response, status = make_request_to_bridge('GET', 'groups')

        if status == 200 and not is_response_error(response):
            # Identify which lights belong to each group and return.
            rooms = []
            for key in response:
                group = response[key]
                if group["type"] == "Room":
                    rooms.append({'name': group['name'], 'lights': group['lights']})
            return {'status': 'Found rooms successfully', 'rooms': rooms}

        # Error if the response from the bridge was bad.
        return {'error': 'An unknown error occurred'}, 500


class Light(Resource):
    """
    Light endpoint.
    """

    def get(self, id):
        """
        The GET endpoint.
        Retrieves the state information of the light with the specified id.
        If light doesn't exist then an error is returned.
        :param id: The id of the light.
        :return: The state of the light or an error if the light doesn't exist.
        """
        # Get the light state if the light exists.
        response, status = make_request_to_bridge('GET', 'lights/{}'.format(id))
        if status == 200 and not is_response_error(response):
            state = response['state']
            return {'status': 'Found light successfully',
                    'light': {'name': response['name'],
                              'state': {'on': state['on'], 'bri': state['bri'], 'hue': state['hue']}}}, 200

        # Return an error if the light doesn't exist.
        return {'error': 'No light with id {}'.format(id)}, 404

    def put(self, id):
        """
        The PUT endpoint.
        Passes the json body of this request to the hue bridge to update the state of the specified light.
        If the light doesn't exist, or there are errors in the body, these errors are returned.
        :param id: The id of the light.
        :return: A success status if the state was updated, or errors if the light doesn't exist or the body was
        incorrect.
        """
        # Confirm light exists before doing anymore work.
        response, status = self.get(id)
        if status == 404:
            return response, status

        # Push body from this request to the bridge PUT request.
        args = request.get_json()
        response, status = make_request_to_bridge('PUT', 'lights/{}/state'.format(id), args)

        # If there were no errors then return success.
        if status == 200 and not is_response_error(response):
            return {'status': 'Updated light state successfully'}, 200

        # If there were errors then return them.
        if isinstance(response, list):
            errors = []
            for i in range(len(response)):
                if 'error' in response[i]:
                    errors.append(response[i]['error']['description'])
            return {'error': errors}, 400
        return response, 500


class Zones(Resource):
    """
    Zones endpoint.
    """

    def post(self):
        """
        The POST endpoint.
        Creates a zone on the hue bridge consisting of the specified lights.
        :return: A success status if the zone was created successfully, otherwise an error.
        """
        # Ensure lights have been provided.
        args = request.get_json()
        if 'lights' not in args:
            return {'error': 'Must provide lights array in request body'}, 400

        # Tell hue to create a zone with the specified lights.
        body = {'lights': args['lights'], 'name': 'Spotify visualisation', 'type': 'Zone'}
        response, status = make_request_to_bridge('POST', 'groups', body)

        if status != 200 or is_response_error(response):
            return {'error': 'Unable to created the zone'}, 500

        return {'status': 'Successfully created zone', 'id': response[0]['success']['id']}, 201


class Zone(Resource):
    """
    Zone endpoint.
    """

    def get(self, id):
        """
        The GET endpoint.
        Retrieves the group information for the specified zone.
        :param id: The id of the zone.
        :return: Information about the specified zone or an error if it doesn't exist.
        """
        response, status = make_request_to_bridge('GET', 'groups/{}'.format(id))

        # Return an error if it doesn't exist.
        if status != 200 or is_response_error(response):
            return {'error': 'Invalid zone id'}, 404

        # Return an error if the group isn't a zone.
        if response['type'] != 'Zone':
            return {'error': 'The id provided does not correspond to a zone, it is instead a {}'.format(
                response['type'])}, 404

        return {'status': 'Found zone successfully',
                'zone': {'name': response['name'], 'lights': response['lights']}}, 200

    def put(self, id):
        """
        The PUT endpoint.
        Passes the json body of this request to the hue bridge to update the state of the all the lights in the
        specified zone.
        If the zone doesn't exist, or there are errors in the body, these errors are returned.
        :param id: The id of the zone.
        :return: A success status if the state was updated, or errors if the zone doesn't exist or the body was
        incorrect.
        """
        # Confirm zone exists before doing anymore work.
        response, status = self.get(id)
        if status == 404:
            return response, status

        # Push body from this request to the bridge PUT request.
        args = request.get_json()
        response, status = make_request_to_bridge('PUT', 'groups/{}/action'.format(id), args)

        # If there were no errors then return success.
        if status == 200 and 'error' not in [item for resp in response for item in resp.keys()]:
            return {'status': 'Updated zone state successfully'}, 200

        # If there were errors then return them.
        errors = []
        for i in range(len(response)):
            if 'error' in response[i]:
                errors.append(response[i]['error']['description'])
        return {'error': errors}, 400

    def delete(self, id):
        """
        The DELETE endpoint.
        Deletes the specified zone.
        :param id: The id of the zone to delete.
        :return: A success status if the zone was deleted successfully, otherwise an error.
        """
        # Ensure a zone with the specified id exists.
        response, status = self.get(id)
        if status == 404:
            return response, status

        # Delete the zone.
        response, status = make_request_to_bridge('DELETE', 'groups/{}'.format(id))
        if status != 200 or is_response_error(response):
            return {'error': 'Unable to delete the zone'}, 500

        return {'status': 'Zone deleted successfully'}, 200


# Start flask.
if __name__ == '__main__':
    # Constants.
    ENDPOINT_AUTHENTICATE = '/authenticate'
    ENDPOINT_CHECK_AUTHENTICATED = '/isauthenticated'
    ENDPOINT_ROOMS = '/rooms'
    ENDPOINT_LIGHT = '/lights/<int:id>'
    ENDPOINT_ZONES = '/zones'
    ENDPOINT_ZONE = '/zones/<int:id>'
    BRIDGE_REQUEST_TIMEOUT = 1

    # Flask setup.
    app = Flask(__name__)
    CORS(app)
    api = Api(app)

    # Pre-request checks.
    app.before_request(ensure_authorisation_parameters_present)

    # After-request utils.
    app.after_request(print_responses)

    # Establish endpoints.
    api.add_resource(Authenticate, ENDPOINT_AUTHENTICATE)
    api.add_resource(CheckAuthenticated, ENDPOINT_CHECK_AUTHENTICATED)
    api.add_resource(Rooms, ENDPOINT_ROOMS)
    api.add_resource(Light, ENDPOINT_LIGHT)
    api.add_resource(Zones, ENDPOINT_ZONES)
    api.add_resource(Zone, ENDPOINT_ZONE)

    # Run flask
    app.run(host='0.0.0.0', port=80, debug=(os.environ['DEBUG'] == 'true'), threaded=False, processes=25)
