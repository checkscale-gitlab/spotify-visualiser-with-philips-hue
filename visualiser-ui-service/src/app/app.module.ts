import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ConsoleComponent } from './components/console.component';
import { ControlPanelComponent } from './components/control-panel.component';
import { HueLightsComponent } from './components/hue-lights.component';
import { RouterModule } from '@angular/router';
import { HueService } from './services/hue.service';
import { SpotifyService } from './services/spotify.service';
import { VisualisationOptionsComponent } from './components/visualisation-options.component';

export function initialiseHueService(hueService: HueService) {
    return (): Promise<any> => {
        return hueService.searchForBridge();
    };
}

export function initialiseSpotifyService(spotifyService: SpotifyService) {
    return (): Promise<any> => {
        return spotifyService.checkForRefreshTokenInQueryParams();
    };
}

@NgModule({
    declarations: [
        AppComponent,
        ControlPanelComponent,
        HueLightsComponent,
        ConsoleComponent,
        VisualisationOptionsComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        RouterModule.forRoot([]),
        FormsModule
    ],
    providers: [
        { provide: APP_INITIALIZER, useFactory: initialiseHueService, deps: [HueService], multi: true },
        { provide: APP_INITIALIZER, useFactory: initialiseSpotifyService, deps: [SpotifyService], multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
