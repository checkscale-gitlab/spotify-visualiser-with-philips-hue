import { Component, OnInit } from '@angular/core';
import { visualisationTypes } from '../enums/visualisation-type.enum';
import { VisualisationType } from '../interfaces/visualiser.interface';
import { VisualiserService } from '../services/visualiser.service';

@Component({
    selector: 'app-visualisation-options',
    templateUrl: './visualisation-options.component.html',
    styleUrls: ['./visualisation-options.component.css']
})
export class VisualisationOptionsComponent implements OnInit {

    readonly visualisationTypes = visualisationTypes; // Required for the HTML to access the visualisation types.

    visualisationType: VisualisationType = visualisationTypes[0];
    brightness: number = 80;

    constructor(private readonly visualiserService: VisualiserService) {

    }

    ngOnInit(): void {
        this.onVisualisationTypeChange();
        this.onBrightnessChange();
    }

    onVisualisationTypeChange(): void {
        this.visualiserService.setVisualisationType(this.visualisationType);
    }

    onBrightnessChange(): void {
        this.visualiserService.setBrightness(this.brightness);
    }
}
