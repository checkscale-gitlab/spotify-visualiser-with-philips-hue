import { OnInit, Component, Input } from '@angular/core';
import { HueService } from '../services/hue.service';
import { SpotifyService } from '../services/spotify.service';
import { HueGroup } from '../interfaces/hue.interface';
import { VisualiserService } from '../services/visualiser.service';

@Component({
    selector: 'app-control-panel',
    templateUrl: './control-panel.component.html',
    styleUrls: ['./control-panel.component.css']
})
export class ControlPanelComponent implements OnInit {

    toggleVisualiserButtonText: string;

    isHueAuthenticated: boolean;
    isSpotifyAuthenticated: boolean;

    constructor(private readonly hueService: HueService,
                private readonly spotifyService: SpotifyService,
                private readonly visualiserService: VisualiserService) {

    }

    ngOnInit(): void {
        this.spotifyService.onIsAuthenticatedChange().subscribe(isAuthenticated => {
            this.isSpotifyAuthenticated = isAuthenticated;
            if (!this.isSpotifyAuthenticated) {
                this.setVisualiserInProgress(false);
            }
        });
        this.hueService.onIsAuthenticatedChange().subscribe(isAuthenticated => {
            this.isHueAuthenticated = isAuthenticated;
            if (!this.isHueAuthenticated) {
                this.setVisualiserInProgress(false);
            }
        });

        this.hueService.checkIsAuthenticated();
        this.spotifyService.generateAccessToken();

        this.toggleVisualiserButtonText = 'Start';
        this.visualiserService.onVisualiserInProgressChange().subscribe(inProgress => {
            if (inProgress) {
                this.toggleVisualiserButtonText = 'Stop';
            } else {
                this.toggleVisualiserButtonText = 'Start';
            }
        });
    }

    setVisualiserInProgress(inProgress: boolean): void {
        if (inProgress && !this.visualiserService.isVisualiserInProgress()) {
            this.visualiserService.startVisualiser();
        } else if (!inProgress && this.visualiserService.isVisualiserInProgress()) {
            this.visualiserService.stopVisualiser();
        }
    }

    authenticateHue(): void {
        this.hueService.authenticate();
    }

    authenticateSpotify(): void {
        this.spotifyService.authenticate();
    }

    logoutSpotify(): void {
        this.spotifyService.logout();
    }

    toggleVisualiser(): void {
        this.setVisualiserInProgress(!this.visualiserService.isVisualiserInProgress());
    }
}
