import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CredentialsService {

    private readonly HUE_USERNAME_KEY = 'hue_username';
    private readonly SPOTIFY_REFRESH_TOKEN_KEY = 'spotify_refresh_token';
    private spotifyAccessToken: string = null;

    getHueUsername(): string {
        return localStorage.getItem(this.HUE_USERNAME_KEY);
    }

    setHueUsername(username: string): void {
        localStorage.setItem(this.HUE_USERNAME_KEY, username);
    }

    clearHueCredentials(): void {
        localStorage.removeItem(this.HUE_USERNAME_KEY);
    }

    getSpotifyRefreshToken(): string {
        return localStorage.getItem(this.SPOTIFY_REFRESH_TOKEN_KEY);
    }

    setSpotifyRefreshToken(refreshToken: string): void {
        localStorage.setItem(this.SPOTIFY_REFRESH_TOKEN_KEY, refreshToken);
    }

    getSpotifyAccessToken(): string {
        return this.spotifyAccessToken;
    }

    setSpotifyAccessToken(accessToken: string): void {
        this.spotifyAccessToken = accessToken;
    }

    clearSpotifyCredentials(): void {
        localStorage.removeItem(this.SPOTIFY_REFRESH_TOKEN_KEY);
    }
}
