import { Injectable } from '@angular/core';
import { HttpClientService } from './http-client.service';
import { UrlService, Api } from './url.service';
import { LoggingService } from './logging.service';
import { CredentialsService } from './credentials.service';
import { Subject, Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { isNullOrUndefined } from 'util';
import { Track, TrackAnalysis } from '../interfaces/spotify.interface';
import { LogType } from '../enums/log-type.enum';

@Injectable({
    providedIn: 'root'
})
export class SpotifyService {

    private authenticated: boolean;
    private authenticatedSubject: Subject<boolean>;

    constructor(private readonly httpClientService: HttpClientService,
                private readonly urlService: UrlService,
                private readonly loggingService: LoggingService,
                private readonly credentialsService: CredentialsService) {
        this.authenticated = false;
        this.authenticatedSubject = new Subject<boolean>();
    }

    checkForRefreshTokenInQueryParams(): Promise<boolean> {
        const url = new URL(window.location.href);
        if (url.searchParams.has('spotify_refresh_token')) {
            if (!url.searchParams.has('state')) {
                this.loggingService.newLog('No state passed back with the spotify refresh token', LogType.ERROR);
                this.credentialsService.clearSpotifyCredentials();
            } else if (url.searchParams.get('state') !== localStorage.getItem('spotifyState')) {
                this.loggingService.newLog('State passed back with the spotify refresh token is invalid', LogType.ERROR);
                this.credentialsService.clearSpotifyCredentials();
            }
            this.credentialsService.setSpotifyRefreshToken(url.searchParams.get('spotify_refresh_token'));
            localStorage.removeItem('spotifyState');
            window.location.href = 'http://' + environment.HOST_IP + ':17010';
        }
        return Promise.resolve(true);
    }

    private responseContainsError(response: any): boolean {
        return !isNullOrUndefined(response) && 'error' in response;
    }

    private getApiHeaders(accessTokenHeader: boolean = true): HttpHeaders {
        let headers = new HttpHeaders();
        if (accessTokenHeader) {
            headers = headers.append('Authorization', 'Bearer ' + this.credentialsService.getSpotifyAccessToken());
        }
        return headers;
    }

    generateAccessToken(): Promise<boolean> {
        const refreshToken = this.credentialsService.getSpotifyRefreshToken();
        if (refreshToken === null) {
            this.setIsAuthenticated(false);
            return Promise.resolve(false);
        }
        const url = this.urlService.getAPIUrl(Api.SPOTIFY, 'authenticate');
        return new Promise(resolve => {
            this.httpClientService.post(url, { refresh_token: refreshToken }, this.getApiHeaders(false)).then(response => {
                if (this.responseContainsError(response)) {
                    this.setIsAuthenticated(false);
                    resolve(false);
                } else {
                    this.credentialsService.setSpotifyAccessToken(response.access_token);
                    this.setIsAuthenticated(true);
                    resolve(true);
                }
            });
        });
    }

    getAuthenticateUrl(): Promise<string> {
        const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let state = '';
        for (let i = 30; i > 0; --i) {
            state += chars[Math.floor(Math.random() * chars.length)];
        }
        localStorage.setItem('spotifyState', state);
        const url = this.urlService.getAPIUrl(Api.SPOTIFY, 'authenticate', { state: localStorage.getItem('spotifyState') });
        return new Promise<string>(resolve => {
            this.httpClientService.get(url, this.getApiHeaders(false)).then((response) => {
                if (this.responseContainsError(response)) {
                    resolve(null);
                } else {
                    resolve(response.url);
                }
            });
        });
    }

    authenticate(): void {
        this.getAuthenticateUrl().then(url => {
            if (url !== null) {
                window.location.href = url;
            } else {
                this.loggingService.newLog('Spotify authentication url is invalid', LogType.ERROR);
            }
        });
    }

    private setIsAuthenticated(authenticated: boolean): void {
        this.authenticated = authenticated;
        this.authenticatedSubject.next(this.authenticated);
        if (!this.authenticated) {
            this.credentialsService.clearSpotifyCredentials();
        }
    }

    isAuthenticated(): boolean {
        return this.authenticated;
    }

    onIsAuthenticatedChange(): Observable<boolean> {
        return this.authenticatedSubject.asObservable();
    }

    logout(): void {
        this.setIsAuthenticated(false);
    }

    getCurrentlyPlayingTrack(): Promise<Track> {
        if (!this.authenticated) {
            return Promise.reject('Unauthenticated');
        }
        const url = this.urlService.getAPIUrl(Api.SPOTIFY, 'currentlyplaying');
        return new Promise<Track>((resolve, reject) => {
            this.httpClientService.get(url, this.getApiHeaders()).then(response => {
                if (this.responseContainsError(response)) {
                    reject(response);
                } else {
                    if (isNullOrUndefined(response)) {
                        resolve(null);
                    } else {
                        const track: Track = {
                            id: response.track_id,
                            isPlaying: response.is_playing,
                            progress: response.progress_ms
                        };
                        resolve(track);
                    }
                }
            });
        });
    }

    getAudioAnalysis(id: string): Promise<TrackAnalysis> {
        if (!this.authenticated) {
            return Promise.reject('Unauthenticated');
        }
        const url = this.urlService.getAPIUrl(Api.SPOTIFY, 'audioanalysis/' + id);
        return new Promise<TrackAnalysis>((resolve, reject) => {
            this.httpClientService.get(url, this.getApiHeaders()).then(response => {
                if (this.responseContainsError(response)) {
                    reject(response);
                } else {
                    const analysis: TrackAnalysis = {
                        beats: response.beats,
                        tempo: response.tempo
                    };
                    resolve(analysis);
                }
            });
        });
    }
}
