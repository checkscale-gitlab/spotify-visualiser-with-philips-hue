import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const HUE_API_BASE_URL = 'http://' + environment.HOST_IP + ':17000';
const SPOTIFY_API_BASE_URL = 'http://' + environment.HOST_IP + ':17001';
const VISUALISER_STREAM_URL = 'http://' + environment.HOST_IP + ':17002';

export enum Api {
    HUE,
    SPOTIFY
}

@Injectable({
    providedIn: 'root'
})
export class UrlService {

    getAPIUrl(api: Api, endpoint: string, params: object = null): string {
        let baseUrl: string;
        switch (api) {
            case Api.HUE:
                baseUrl = HUE_API_BASE_URL;
                break;
            case Api.SPOTIFY:
                baseUrl = SPOTIFY_API_BASE_URL;
                break;
            default:
                baseUrl = '';
                break;
        }
        let url = baseUrl + '/' + endpoint;
        if (params !== null) {
            url += '?';
            for (const key of Object.keys(params)) {
                url += key + '=' + params[key] + '&';
            }
        }
        return url;
    }

    getVisualiserStreamUrl(): string {
        return VISUALISER_STREAM_URL;
    }
}
