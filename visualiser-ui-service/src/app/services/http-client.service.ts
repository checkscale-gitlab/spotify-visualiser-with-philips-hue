import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoggingService } from './logging.service';
import { LogType } from '../enums/log-type.enum';

@Injectable({
    providedIn: 'root'
})
export class HttpClientService {

    constructor(private readonly httpClient: HttpClient,
                private readonly loggingService: LoggingService) {

    }

    private logErrorsThenReturn(responsePromise: Promise<any>): Promise<any> {
        return responsePromise.then((response) => {
            if (!(response === null || response === undefined)) {
                if ('error' in response) {
                    this.loggingService.newLog(response.error, LogType.ERROR);
                }
            }
            return response;
        }, (rejection) => {
            if (!('error' in rejection)) {
                this.loggingService.newLog(rejection, LogType.ERROR);
            } else if ('error' in rejection.error) {
                this.loggingService.newLog(rejection.error.error, LogType.ERROR);
            } else {
                this.loggingService.newLog(rejection.error, LogType.ERROR);
            }
            return rejection;
        });
    }

    get(url: string, headers: HttpHeaders = null): Promise<any> {
        return this.logErrorsThenReturn(this.httpClient.get(url, { headers }).toPromise());
    }

    post(url: string, body: any = null, headers: HttpHeaders = null): Promise<any> {
        return this.logErrorsThenReturn(this.httpClient.post(url, body, { headers }).toPromise());
    }

    put(url: string, body: any = null, headers: HttpHeaders = null): Promise<any> {
        return this.logErrorsThenReturn(this.httpClient.put(url, body, { headers }).toPromise());
    }
}
