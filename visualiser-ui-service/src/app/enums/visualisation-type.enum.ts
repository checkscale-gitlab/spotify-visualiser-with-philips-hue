import { VisualisationType } from '../interfaces/visualiser.interface';

export const visualisationTypes: VisualisationType[] = [
    {
        id: 'ON_BEAT',
        displayName: 'On Beat'
    }
];
