export interface HueLight {
    id: number;
    name: string;
    state: object;
    selected: boolean;
}

export interface HueGroup {
    name: string;
    lights: HueLight[];
}
