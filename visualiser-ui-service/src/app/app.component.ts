import { Component } from '@angular/core';
import { HueLight } from './interfaces/hue.interface';
import { HueService } from './services/hue.service';
import { SpotifyService } from './services/spotify.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    isHueAuthenticated: boolean;
    isSpotifyAuthenticated: boolean;

    constructor(private readonly hueService: HueService,
                private readonly spotifyService: SpotifyService) {
        this.hueService.onIsAuthenticatedChange().subscribe(isAuthenticated => {
            this.isHueAuthenticated = isAuthenticated;
        });
        this.spotifyService.onIsAuthenticatedChange().subscribe(isAuthentictaed => {
            this.isSpotifyAuthenticated = isAuthentictaed;
        });
    }
}
